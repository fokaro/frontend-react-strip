import { useState } from "react";
import { useNavigate } from "react-router-dom";
import CheckoutForm from "./CheckoutForm";

const CreateMarchandise = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");

  const changeName = (e) => {
    let Name = e.target.value;
    setName(Name);
  };

  const changeEmail = (e) => {
    let email = e.target.value;
    setEmail(email);
  };

  return (
    <body>
        <main className="main-content  mt-0">
          <section>
            <div className="page-header min-vh-100">
              <div className="container">
                <div className="row">

                  <div className="col-xl-10 col-lg-5 col-md-7 d-flex flex-column ms-auto me-auto ms-lg-auto me-lg-5">
                    <div className="card card-plain">
                      <div className="card-header">
                        <h2 className="text-success font-weight-bolder">
                          Registration form
                        </h2>
                        <p className="mb-0">Enter Your Card to complete this registration process</p>
                      </div>
                      <div className="card-body">
                        <form role="form">
                          <div className="input-group input-group-outline mb-3">
                            <label className="">Name</label>
                            <input
                              type="text"
                              className=""
                              aria-label="Name"
                              onChange={changeName}
                            />
                          </div>
                          <div className="input-group input-group-outline mb-3">
                            <label className="">Email</label>
                            <input
                              type="email"
                              className=""
                              aria-label="Email"
                              onChange={changeEmail}
                            />
                          </div>
                          <CheckoutForm name={name} email={email}/>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>

    </body>
  );
};
export default CreateMarchandise;
