import React, {useState, useEffect} from "react";
import {
    CardElement,
    useStripe,
    useElements
} from "@stripe/react-stripe-js";

import "./pay.css";

export default function CheckoutForm(props) {
    const [succeeded, setSucceeded] = useState(false);
    const [error, setError] = useState(null);
    const [processing, setProcessing] = useState(false);
    const [disabled, setDisabled] = useState(true);
    const stripe = useStripe();
    const elements = useElements();

    const {name, email} = props

    const cardStyle = {
        style: {
            base: {
                color: "#32325d",
                fontFamily: 'Arial, sans-serif',
                fontSmoothing: "antialiased",
                fontSize: "16px",
                "::placeholder": {
                    color: "#32325d"
                }
            },
            invalid: {
                fontFamily: 'Arial, sans-serif',
                color: "#fa755a",
                iconColor: "#fa755a"
            }
        }
    };

    const handleChange = async (event) => {
        // Listen for changes in the CardElement
        // and display any errors as the customer types their card details
        setDisabled(event.empty);
        setError(event.error ? event.error.message : "");
    };

    const handleSubmit = async ev => {

        ev.preventDefault();

        setProcessing(true);
        let options = {
            method: "POST",
            headers: {"Content-type": "application/json;charset=utf-8"},
            body: JSON.stringify({name: name, email: email}),
        };
        const data = await fetch(
            "http://localhost:8000/api/create/intent",
            options
        ).then((response) => response.json());

        // console.log(data);

        const paymentIntent = JSON.parse(data);

        //console.log("le bon json")
        const payload = await stripe.confirmCardPayment(paymentIntent.client_secret, {
            payment_method: {
                card: elements.getElement(CardElement)
            }
        });

        console.log(payload);

        const dataT = payload.paymentIntent;

        if (dataT) {
                console.log("il y a le payload")
                const toBackend = {
                    client_secret: dataT.id,
                    payment_method: dataT.payment_method
                }

                let options2 = {
                    method: "POST",
                    headers: {"Content-type": "application/json;charset=utf-8"},
                    body: JSON.stringify(toBackend),
                };

                const data2 = await fetch(
                    "http://localhost:8000/api/confirm/intent",
                    options2
                ).then((response) => response.json());
                if (data2) {
                    alert("Okay")
                }
        }

        if (payload.error) {
            setError(`Payment failed ${payload.error.message}`);
            setProcessing(false);
        } else {
            setError(null);
            setProcessing(false);
            setSucceeded(true);
        }
    };

    return (
        <>          <CardElement id="card-element" options={cardStyle} onChange={handleChange}/>
            <div className="text-center">
                <button
                    disabled={processing || disabled || succeeded}
                    id="submit"
                    onClick={handleSubmit}
                    type="button"
                    className="btn btn-lg bg-gradient-primary btn-lg w-100 mt-4 mb-0"
                >
                     <span id="button-text">
          {processing ? (
              <div className="spinner" id="spinner"></div>
          ) : (
              "Pay now"
          )}
        </span>
                </button>
            </div>
            {/* Show any error that happens when processing the payment */}
            {error && (
                <div className="card-error" role="alert">
                    {error}
                </div>
            )}
            {/* Show a success message upon completion */}
            <p className={succeeded ? "result-message" : "result-message hidden"}>
                Payment succeeded, see the result in your
                <a
                    href={`https://dashboard.stripe.com/test/payments`}
                >
                    {" "}
                    Stripe dashboard.
                </a> Refresh the page to pay again.
            </p></>
    );
}