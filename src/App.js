import React, {Component} from "react";
import {
    BrowserRouter as Router, Route, Routes
} from "react-router-dom";
import CreateMarchandise from "./components/CreateMarchandise";
import {Elements} from "@stripe/react-stripe-js";
import {loadStripe} from "@stripe/stripe-js";

const promise = loadStripe("pk_test_51MQT2pKeHcCj15fGjmJhYreLXzKg701UOkjZzn28siy5O0yyPnwl83YblJeW712k4oYWSZFJ5TzQK9J7715nwART00xZpilkVQ");

class App extends Component {
    render() {
        return (
            <Elements stripe={promise}>
                <Router>
                    <Routes>
                        <Route path="/" element={<CreateMarchandise/>}/>
                    </Routes>
                </Router>
            </Elements>
        );
    }
}

export default App;